import React, {createContext, useReducer} from "react";

import AppReducer from "./AppReducer";

const initialState = {
    books: [
        {id: 1, title: "Book 1"},
        {id: 2, title: "Book 2"},
        {id: 3, title: "Book 3"},
        {id: 4, title: "Book 4"},
    ]
};

// create context
export const GlobalContext = createContext(initialState);

// provider component
export const GlobalProvider = ({children}) => {
    const [state, dispatch] = useReducer(AppReducer, initialState);

    function deleteBook(id) {
        dispatch({
            type: "DELETE_BOOK",
            payload: id
        });
    }

    function addBook(book) {
        dispatch({
            type: "ADD_BOOK",
            payload: book
        });
    }

    function editBook(book) {
        dispatch({
            type: "EDIT_BOOK",
            payload: book
        });
    }

    return (
        <GlobalContext.Provider value={{
            books: state.books,
            deleteBook,
            addBook,
            editBook
        }}>
            {children}
        </GlobalContext.Provider>
    );
}