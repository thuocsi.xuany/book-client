import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { ListGroup, ListGroupItem, Button } from "reactstrap";
import { GlobalContext } from "../context/GlobalState";

export const BookList = () => {
  const { books,deleteBook } = useContext(GlobalContext);
  return (
    <ListGroup className="mt-4">
      {books.map((book) => {
        return (
          <ListGroupItem key={book.id} className="d-flex">
            <strong>{book.title}</strong>
            <div className="ms-auto">
              <Link
                to={`/edit/${book.id}`}
                className="btn btn-sm btn-warning me-1"
              >
                Edit
              </Link>
              <Button color="danger" size="sm" onClick={()=>deleteBook(book.id)}>
                Delete
              </Button>
            </div>
          </ListGroupItem>
        );
      })}
    </ListGroup>
  );
};
