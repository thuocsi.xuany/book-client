import React, { useContext,useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";

import { GlobalContext } from "../context/GlobalState";
export const AddBook = () => {
  const { addBook } = useContext(GlobalContext);
  const [title,setTitle] = useState("");
  const navigate = useNavigate();
  const onSubmit = (e) => {
    e.preventDefault();
    const book = {
      id: 6,
      title,
    };
    addBook(book);
    setTitle("");
    navigate("/");
  };

  const onChange = (e) => {
    setTitle(e.target.value);
  };
  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label for="title">Title</Label>
        <Input type="text" name="title" id="title" placeholder="Enter title" value={title} onChange={onChange}/>
      </FormGroup>
      <Button type="submit">Submit</Button>
      <Link to="/" className="btn btn-danger ms-2">
        Cancel
      </Link>
    </Form>
  );
};
