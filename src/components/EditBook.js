import React, { useState, useContext, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { Form, FormGroup, Label, Input, Button } from "reactstrap";
import { GlobalContext } from "../context/GlobalState";

export const EditBook = () => {
  const { books, editBook } = useContext(GlobalContext);
  const [selectedBook, setSelectedBook] = useState({ id: "", title: "" });
  const navigate = useNavigate();

  const currentUserId = useParams().id;
  useEffect(() => {
    const bookId = currentUserId;
    const selectedBook = books.find((book) => book.id === Number(bookId));

    setSelectedBook(selectedBook);
}, [currentUserId, books]);

const onSubmit = (e) => {
    e.preventDefault();
    editBook(selectedBook);
    navigate("/");
  };

  const onChange = (e) => {
    setSelectedBook({ ...selectedBook, [e.target.name]: e.target.value });
  };
  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <Label for="title">Title</Label>
        <Input
          type="text"
          name="title"
          id="title"
          value={selectedBook.title}
          placeholder="Enter title"
          onChange={onChange}
          required
        />
      </FormGroup>
      <Button type="submit">Edit name</Button>
      <Link to="/" className="btn btn-danger ms-2">
        Cancel
      </Link>
    </Form>
  );
};
