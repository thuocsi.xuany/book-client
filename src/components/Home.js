import React from "react";
import { Heading } from "./Heading";
import { BookList } from "./BookList";
export const Home = () => {
  return (
    <div>
      <Heading />
      <BookList />
    </div>
  );
};
